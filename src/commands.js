function move(currentPosition, limit) {
  let x = parseInt(currentPosition[0]);
  let y = parseInt(currentPosition[1]);
  switch (currentPosition[2]) {
    case 'N':
      y++;
      break;
    case 'E':
      x++;
      break;
    case 'S':
      y--;
      break;
    case 'W':
      x--;
      break;
    default:
      break;
  }
  if(
    (x > limit[0] || x < 0)
    || (y > limit[1] || y < 0)
  )
    return currentPosition;
  else
    return [x, y];
}

function turn(orientation, direction) {
  let newOrientation;
  switch (orientation) {
    case 'N':
      newOrientation = direction === 'D' ? 'E' : 'W';
      break;
    case 'E':
      newOrientation = direction === 'D' ? 'S' : 'N';
      break;
    case 'S':
      newOrientation = direction === 'D' ? 'W' : 'E';
      break;
    case 'W':
      newOrientation = direction === 'D' ? 'N' : 'S';
      break;
    default:
      break;
  }
  return newOrientation;
}

module.exports = {
  turn,
  move
}