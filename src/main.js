const readline = require('readline');
const fs = require('fs');

const { move, turn } = require('./commands');

init();

function init() {
  const [limitString, ...lines] = readFile();
  const limit = limitString.split(' ');
  const mowers = getMowers(lines);
  mowers.map((m, i) => {
    console.log("START Mower ", i);
    let position = m.initial;
    m.commands.map(c => {
      console.log("position mower ", i, " => ", position, "comm", c);
      position = nextPosition(position, c, limit);
    })
    console.log("END Mower ", i, " end position => ", position);
  })
}

function readFile() {
  return fs.readFileSync(__dirname+'/instructions.txt').toString().split("\n");
}

function getMowers(lines) {
  return  lines.reduce(function(result, value, index, array) {
    if (index % 2 === 0)
      result.push(
        {
          initial: array[index].split(' '),
          commands: array[index+1].split('')
        });
    return result;
  }, []);
}

function nextPosition(currentPosition, command, limit) {
  let [x, y, direction] = currentPosition;
  switch (command){
    case 'A':
      [x, y] = move(currentPosition, limit);
      break;
    case 'D':
    case 'G':
      direction = turn(currentPosition[2], command);
      break;
    default:
      break;
  }
  return [x, y, direction]
}

